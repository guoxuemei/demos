//
//  ADCollectionViewCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/16.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NSString * (^ImageURLBlock)(void);

@interface ADCollectionViewCell : UICollectionViewCell

//返回图片的 urlString
@property (nonatomic ,copy) ImageURLBlock imageURLBlock;

@end
