//
//  CDUserDetailCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  抄单用户明细
 */
@interface CDUserDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;


@end
