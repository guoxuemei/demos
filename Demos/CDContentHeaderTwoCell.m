//
//  CDContentHeaderTwoCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDContentHeaderTwoCell.h"

@implementation CDContentHeaderTwoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//- (void)drawRect:(CGRect)rect {
//    
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    
//    CGContextSetLineWidth(context, 0.5);
//    CGContextSetRGBStrokeColor(context, 110/255.0, 110/255.0, 110/255.0, 1);
//    
//    CGContextMoveToPoint(context, self.contentView.left, self.self.contentView.bottom );
//    CGContextAddLineToPoint(context, self.contentView.right, self.contentView.bottom);
//    
//    CGContextMoveToPoint(context, self.textLabel1.right, self.textLabel1.top );
//    CGContextAddLineToPoint(context, self.textLabel1.right, self.textLabel1.bottom);
//    
//    CGContextMoveToPoint(context, self.textLabel2.right, self.textLabel2.top );
//    CGContextAddLineToPoint(context, self.textLabel2.right, self.textLabel2.bottom);
//    
//    CGContextMoveToPoint(context, self.textLabel3.right, self.textLabel3.top );
//    CGContextAddLineToPoint(context, self.textLabel3.right, self.textLabel3.bottom);
//    CGContextStrokePath(context);
//    
//}

- (IBAction)alertBtnTap:(id)sender {
    
    if (self.tapBlock) {

        self.tapBlock(sender);
        
    }
}
@end
