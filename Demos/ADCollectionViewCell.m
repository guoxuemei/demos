//
//  ADCollectionViewCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/16.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "ADCollectionViewCell.h"

@interface ADCollectionViewCell ()

@property (nonatomic ,strong) UIImageView *adImageView;

@end

@implementation ADCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.adImageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        self.adImageView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.adImageView];
        
        self.contentView.backgroundColor = [UIColor cyanColor];
    }
    
    return self;
}
- (void)setImageURLBlock:(ImageURLBlock)imageURLBlock {
    
//    [self.adImageView setImageWithURL:]
    NSString *imageURL = imageURLBlock();
    [self.adImageView setImage:[UIImage imageNamed:imageURL]];
    
}
@end
