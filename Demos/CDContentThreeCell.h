//
//  CDContentThreeCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/23.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  混投了多种玩法的投注,主要涉及竞彩混投,单关,篮彩混投,单关(必须混投了两种玩法及以上)
 */
@interface CDContentThreeCell : UITableViewCell<UITableViewDelegate , UITableViewDataSource>

@property (nonatomic , strong)NSArray *dataArray;

@property (weak, nonatomic) IBOutlet UILabel *changCiLabel;
@property (weak, nonatomic) IBOutlet UILabel *duiZhenLabel;
@property (weak, nonatomic) IBOutlet UITableView *detailTableV;

@end
