//
//  CDFaQiInfoCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDFaQiInfoCell.h"

@implementation CDFaQiInfoCell

- (void)awakeFromNib {

    UIColor *color = [UIColor colorWithRed:0.141 green:0.576 blue:0.831 alpha:1.0];

    self.cdRecordBtn.layer.borderColor = color.CGColor;
    self.cdRecordBtn.layer.borderWidth = 1.0;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)toChaoDanRecord:(id)sender {
    
    //抄单记录
    if (self.recordBlock) {
        self.recordBlock();
    }
    
}

@end
