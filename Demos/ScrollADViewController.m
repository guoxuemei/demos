//
//  ScrollADViewController.m
//  Demos
//
//  Created by lixiaofei on 15/11/16.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "ScrollADViewController.h"
#import "ADPageView.h"

@interface ScrollADViewController ()


@end

@implementation ScrollADViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"广告页无限滚动";
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    ADPageView *pageView = [[ADPageView alloc]initWithFrame:CGRectMake(0, 100, SCREEN_WIDTH, 65)];
    pageView.itemsArray = [NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5", nil];
    pageView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"198ad"]];
    [self.view addSubview:pageView];
    
    pageView.tapBlock = ^(NSInteger index){
        NSLog(@"%d",index);
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
