//
//  CDFanganCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  抄单方案信息Cell
 */
@interface CDFanganCell : UITableViewCell
//方案编号
@property (weak, nonatomic) IBOutlet UILabel *fanganCode;
//开始时间
@property (weak, nonatomic) IBOutlet UILabel *startLabel;
//截止时间
@property (weak, nonatomic) IBOutlet UILabel *endLabel;

//提示文字1,2,3 显示数据1,2,3
@property (weak, nonatomic) IBOutlet UILabel *hint1Label;
@property (weak, nonatomic) IBOutlet UILabel *hint2Label;
@property (weak, nonatomic) IBOutlet UILabel *hint3Label;

@property (weak, nonatomic) IBOutlet UILabel *text1Label;
@property (weak, nonatomic) IBOutlet UILabel *text2Label;
@property (weak, nonatomic) IBOutlet UILabel *text3Label;


@end
