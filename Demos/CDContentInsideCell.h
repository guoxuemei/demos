//
//  CDContentInsideCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/23.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  多种混投方式右侧的投注玩法展示
 */
@interface CDContentInsideCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *wanFaLabel;
@property (weak, nonatomic) IBOutlet UILabel *touZhuLabel;


@end
