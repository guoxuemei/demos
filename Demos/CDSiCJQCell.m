//
//  CDSiCJQCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/20.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDSiCJQCell.h"

@implementation CDSiCJQCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.5);
    CGContextSetRGBStrokeColor(context, 200.0/255.0, 200.0/255.0, 200.0/255.0, 1);
    
    CGContextMoveToPoint(context, self.num1Label.left, self.num1Label.bottom);
    CGContextAddLineToPoint(context, self.num1Label.right, self.num1Label.bottom);
    
    CGContextMoveToPoint(context, self.type1Label.left, self.type1Label.bottom );
    CGContextAddLineToPoint(context, self.result1Label.right, self.result1Label.bottom);
    
    CGContextMoveToPoint(context, self.num1Label.right, self.num1Label.top );
    CGContextAddLineToPoint(context, self.num2Label.right, self.num2Label.bottom);
    
    CGContextMoveToPoint(context, self.duiZhenLabel.right, self.duiZhenLabel.top );
    CGContextAddLineToPoint(context, self.duiZhenLabel.right, self.duiZhenLabel.bottom);
    
    CGContextMoveToPoint(context, self.type1Label.right, self.type1Label.top );
    CGContextAddLineToPoint(context, self.type2Label.right, self.type2Label.bottom);
    
    CGContextMoveToPoint(context, self.touZhu1Label.right, self.touZhu1Label.top );
    CGContextAddLineToPoint(context, self.touZhu2Label.right, self.touZhu2Label.bottom);
    
    CGContextMoveToPoint(context, self.contentView.left, self.contentView.bottom);
    CGContextAddLineToPoint(context, self.contentView.right, self.contentView.bottom);
    CGContextStrokePath(context);

    
}
@end
