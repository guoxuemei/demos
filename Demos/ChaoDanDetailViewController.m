//
//  ChaoDanDetailViewController.m
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "ChaoDanDetailViewController.h"
#import "CDHeaderView.h"
#import "CDFaQiInfoCell.h"
#import "CDFanganCell.h"
#import "CDContentHeadOneCell.h"
#import "CDContentHeaderTwoCell.h"
#import "CDContentHeaderThreeCell.h"

#import "CDContentOneCell.h"
#import "CDContentTwoCell.h"
#import "CDContentThreeCell.h"
#import "CDUserDetailCell.h"
#import "CDSiCJQCell.h"

#define CDContentHeaderHeight @[@"90",@"70",@"50"]

//Cell文件的选择
//0 :四列普通,1:五列普通,2:四列复杂情况,多种混投方式,使用 tableview 3:四场进球及六场半全场
typedef NS_ENUM(NSInteger , ContentCellType) {
    ContentCellTypeOne = 0,     //CDContentOneCell
    ContentCellTypeTwo,         //CDContentTwoCell
    ContentCellTypeThree,       //CDContentThreeCell
    ContentCellTypeSiCJQ,       //CDSiCJQCell
};

@interface ChaoDanDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

//例: @[@"场次",@"主队VS客队",@"玩法",@"投注"]等
@property (nonatomic ,copy)NSArray *contentHeaderTitleArray;
@property (nonatomic ,assign)ContentCellType  contentCellType;
@end

@implementation ChaoDanDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"方案详情";
    
    self.userType = UserTypeFaQiRen;
    self.openType = OpenTypeNonPublic;
    self.lotteryId = 208;
    self.lotteryType = [self getLotteryTypeWithLotteryId:self.lotteryId];
    
    self.view.backgroundColor =  [UIColor colorWithRed:244.0/255 green:244.0/255 blue:242.0/255 alpha:1];// kBGColor
    
    CDHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDHeaderView class]) owner:nil options:nil] objectAtIndex:0];
    headerView.frame = CGRectMake(10, 64 + 10, SCREEN_WIDTH - 20, 90*iPhone6Ratio);
    [self.view addSubview:headerView];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(10, headerView.bottom + 10, SCREEN_WIDTH -  20, SCREEN_HEIGHT - headerView.bottom - 10 - 44) style:UITableViewStyleGrouped];
    tableView.sectionFooterHeight = 0;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate and Datasource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //发起人五个,非发起人的,方案公开的4个,未公开的三个
    if (self.userType == UserTypeFaQiRen ) {
        return 5;
    } else {
        if (self.openType == OpenTypePublic) {
            return 4;
        } else {
            return 3;
        }
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    if (section != 2) {
        return 1;
    } else {
        if (self.userType == UserTypeChaoDanRen && self.userType == OpenTypeNonPublic) {
            return 1;
        }
        return 1+5;//方案内容区

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 70 * iPhone6Ratio;
            break;
        case 1:
            if (self.userType == UserTypeFaQiRen || (self.userType == UserTypeChaoDanRen && self.openType == OpenTypeNonPublic)) {
                return 70 * iPhone6Ratio;
            }
            return 90 * iPhone6Ratio;
            break;
        case 2:
            if (indexPath.row == 0) {
                if (self.userType == UserTypeChaoDanRen && self.openType == OpenTypeNonPublic) {
                    return 60*iPhone6Ratio;//未公开的情况
                }
                
                return [(NSString *)CDContentHeaderHeight[self.lotteryType] intValue] * iPhone6Ratio;//已公开的情况
            } else {
                //每行赛事信息,动态高度
                return 60 * iPhone6Ratio;
            }
            
            break;
        case 3://拆单明细与抄单用户
        case 4:
            return 44 * iPhone6Ratio;
            break;
        default:
            return 0;
            break;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.1;//0的话会出系统默认的一个很高的 header
    }
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        static NSString *faqiIdentifier = @"FaQiIdentifier";
        CDFaQiInfoCell *cell = (CDFaQiInfoCell *)[tableView dequeueReusableCellWithIdentifier:faqiIdentifier];
        if (cell == nil) {
            cell = (CDFaQiInfoCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDFaQiInfoCell class]) owner:nil options:nil] lastObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.nameLabel.text = @"这里填发起人";
        cell.awardCountLabel.text = @"100万元";
        cell.recordBlock = ^{
          //TODO:去往抄单记录
//            RecordViewController  *record = [[RecordViewController alloc]init];
//            record.schusername = self.username;
//            recored.vcType = ChaoDanRecord;
//            [self.navigationController pushViewController:record animated:YES];
            
        };
        return cell;
    } else if (indexPath.section == 1) {
        static NSString *fanganIdentifier = @"FangAnIdentifier";
        CDFanganCell *cell = (CDFanganCell *)[tableView dequeueReusableCellWithIdentifier:fanganIdentifier];
        if (cell == nil) {
            cell = (CDFanganCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDFanganCell class]) owner:nil options:nil] lastObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [self configFanganInfo:cell];
        
        return cell;
        
    } else if (indexPath.section == 2) {
        BOOL type = [self getHeaderInfoWithlotteryId:self.lotteryId];
        NSLog(@"%d,%ld",type,(long)_contentCellType);
        if (indexPath.row == 0) {
            if (self.userType == UserTypeChaoDanRen && self.openType == OpenTypeNonPublic) {
                //抄单人非公开方案内容
                static NSString *contentHeaderOneIdentifier = @"ContentHeaderOneIdentifier";
                CDContentHeadOneCell *cell = (CDContentHeadOneCell *)[tableView dequeueReusableCellWithIdentifier:contentHeaderOneIdentifier];
                if (cell == nil) {
                    cell = (CDContentHeadOneCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDContentHeadOneCell class]) owner:nil options:nil] lastObject];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                return cell;
            } else {
                
                if (type == 0) {
                    static NSString *contentHeaderTwoIdentifier = @"ContentHeaderTwoIdentifier";
                    CDContentHeaderTwoCell *cell = (CDContentHeaderTwoCell *)[tableView dequeueReusableCellWithIdentifier:contentHeaderTwoIdentifier];
                    if (cell == nil) {
                        cell = (CDContentHeaderTwoCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDContentHeaderTwoCell class]) owner:nil options:nil] lastObject];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                    [self configHeaderTwoCell:cell];
                    
                    return cell;
                    
                } else  {
                    static NSString *contentHeaderThreeIdentifier = @"ContentHeaderThreeIdentifier";
                    CDContentHeaderThreeCell *cell = (CDContentHeaderThreeCell *)[tableView dequeueReusableCellWithIdentifier:contentHeaderThreeIdentifier];
                    if (cell == nil) {
                        cell = (CDContentHeaderThreeCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDContentHeaderThreeCell class]) owner:nil options:nil] lastObject];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                    [self configHeaderThreeCell:cell];
                    
                    return cell;
                }
                
            }

        } else {
            //配置内容 Cell
           
            switch (_contentCellType) {
                case ContentCellTypeOne:
                {
                    static NSString *contentOneIdentifier = @"ContentOneIdentifier";
                    CDContentOneCell *cell = (CDContentOneCell *)[tableView dequeueReusableCellWithIdentifier:contentOneIdentifier];
                    if (cell == nil) {
                        cell = (CDContentOneCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDContentOneCell class]) owner:nil options:nil] lastObject];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                    [self configContentOneCell:cell indexPath:indexPath];
                    return cell;
                }
                    break;
                case ContentCellTypeTwo:{
                    static NSString *contentTwoIdentifier = @"ContentTwoIdentifier";
                    CDContentTwoCell *cell = (CDContentTwoCell *)[tableView dequeueReusableCellWithIdentifier:contentTwoIdentifier];
                    if (cell == nil) {
                        cell = (CDContentTwoCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDContentTwoCell class]) owner:nil options:nil] lastObject];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                    [self configContentTwoCell:cell indexPath:indexPath];
                    return cell;

                }
                    break;
                case ContentCellTypeThree:{
                    static NSString *contentThreeIdentifier = @"ContentThreeIdentifier";
                    CDContentThreeCell *cell = (CDContentThreeCell *)[tableView dequeueReusableCellWithIdentifier:contentThreeIdentifier];
                    if (cell == nil) {
                        cell = (CDContentThreeCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDContentThreeCell class]) owner:nil options:nil] lastObject];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                    [self configContentThreeCell:cell indexPath:indexPath];
                    return cell;
                    
                }
                    break;
                case ContentCellTypeSiCJQ:{
                    static NSString *contentSiCJQIdentifier = @"ContentSiCJQIdentifier";
                    CDSiCJQCell *cell = (CDSiCJQCell *)[tableView dequeueReusableCellWithIdentifier:contentSiCJQIdentifier];
                    if (cell == nil) {
                        cell = (CDSiCJQCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDSiCJQCell class]) owner:nil options:nil] lastObject];
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                    [self configContentSiCJQCell:cell indexPath:indexPath];
                    return cell;
                    
                }
                    break;
                    
                default:
                    break;
            }
            
        }
    } else {
        //拆单明细与抄单用户
        static NSString *cellIdentifier = @"CDUserDetailCellIdentifier";
        CDUserDetailCell *cell = (CDUserDetailCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = (CDUserDetailCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDUserDetailCell class]) owner:nil options:nil] lastObject];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (indexPath.section == 3) {
            cell.leftLabel.text = @"拆单明细";
            cell.rightLabel.hidden = YES;
        } else {
            cell.leftLabel.text = @"抄单用户";
        }
        
        return cell;
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 3) {
        //拆单明细
    } else if (indexPath.section == 4){
        //抄单用户
    }
}

/*!
 *  @brief  根据彩种类型决定使用哪种类型的 Cell 以及相应的标题文本,返回值0 代表使用 CDContentHeaderTwoCell,1代表使用CDContentHeaderThreeCell
 *头部文本分类,一共九种,四种四列的,五种五列的
 ①场次 主队VS客队 玩法 投注 			竞彩_混合投注,单关
 ②场次 主队VS客队 投注 彩果 			竞彩_胜平负,竞彩_比分,竞彩_总进球,竞彩_半全场,(传统足球)14场胜负彩,胜负彩任9场
 ③场次 主队VS客队 让球 投注 彩果 		竞彩_让球胜平负
 ④场次 客队VS主队 玩法 投注        	篮彩_混合投注,单关
 ⑤场次 客队VS主队 投注 赛果			篮彩_胜分差,篮彩_胜负
 ⑥场次 客队VS主队 让分 投注 赛果		篮彩_让分胜负
 ⑦场次 客队VS主队 预设总分 投注 赛果		篮彩_大小分
 
 ⑧场次 客队VS主队 主/客 投注 彩果		四场进球彩
 ⑨场次 客队VS主队 半/全 投注 彩果		六场半全场
 ⑦⑧和⑨的后三部分区域等比划分
 
 */
//这个方法在返回数据后就应该确认一次
- (int)getHeaderInfoWithlotteryId:(int)lotteryId {
    
    int type;
   
    if (lotteryId == 208 || lotteryId == 889) {//①
        
        type = 0;
        //TODO:判断竞彩是不是多种混投方式,是的话,ContentCellTypeThree
        _contentCellType = ContentCellTypeThree;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"主队VS客队",@"玩法",@"投注", nil];
        
    } else if (lotteryId == 209 || lotteryId == 211 || lotteryId == 212 || lotteryId == 213 || lotteryId == 108 || lotteryId == 109){//②
        
        type = 0;
        _contentCellType = ContentCellTypeOne;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"主队VS客队",@"投注",@"彩果", nil];
        
    } else if (lotteryId == 210){//③
        
        type = 1;
        _contentCellType = ContentCellTypeTwo;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"主队VS客队",@"让球",@"投注",@"彩果", nil];
        
    } else if (lotteryId == 218 || lotteryId == 998){//④
        
        type = 0;
        //TODO:判断篮彩是不是多种混投方式,是的话,ContentCellTypeThree
        _contentCellType = ContentCellTypeOne;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"客队VS主队",@"玩法",@"投注", nil];
        
    } else if (lotteryId == 217 || lotteryId == 216){//⑤
        
        type = 0;
        _contentCellType = ContentCellTypeOne;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"客队VS主队",@"投注",@"赛果", nil];
        
    } else if (lotteryId == 214){//⑥
        
        type = 1;
        _contentCellType = ContentCellTypeTwo;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"客队VS主队",@"让分",@"投注",@"赛果", nil];
        
    } else if (lotteryId == 215){//⑦
        
        type = 1;
        _contentCellType = ContentCellTypeTwo;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"客队VS主队",@"预设总分",@"投注",@"赛果", nil];
        
    } else if (lotteryId == 111){//⑧
        
        type = 1;
        _contentCellType = ContentCellTypeSiCJQ;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"客队VS主队",@"主/客",@"投注",@"彩果", nil];
        
    } else if (lotteryId == 110){//⑨
        
        type = 1;
        _contentCellType = ContentCellTypeSiCJQ;
        _contentHeaderTitleArray = [NSArray arrayWithObjects:@"场次",@"客队VS主队",@"半/全",@"投注",@"彩果", nil];
        
    }
    
    return type;
    
}
#pragma mark - 配置section1,以及方案内容头部显示 Methods
- (void)configFanganInfo:(CDFanganCell *)cell {
    
    if (self.userType == UserTypeFaQiRen) {
        cell.hint3Label.hidden = YES;
        cell.text3Label.hidden = YES;
        cell.text1Label.text = @"方案倍数xxx";
        cell.text2Label.text = @"抄单提成xxxx";
    } else {
        if (self.openType == OpenTypePublic) {
            cell.text1Label.text = @"方案倍数1";
            cell.text2Label.text = @"抄单提成1";
            cell.text3Label.text = @"抄单份数1";
        } else {
            cell.hint1Label.text = @"抄单份数";
            cell.hint2Label.text = @"抄单提成";
            cell.text1Label.text = @"抄单份数xxx";
            cell.text2Label.text = @"抄单提成1000%";
            cell.text3Label.hidden = YES;
            cell.hint3Label.hidden = YES;
        }
    }
}
- (void)configHeaderTwoCell:(CDContentHeaderTwoCell *)cell {
    
    if (self.lotteryType == LotteryTypeZuCai) {
        cell.topSpaceCon.constant = 41 * iPhone6Ratio;
        
        if (_lotteryId == 889) {
            //竞彩单关没有红球球
            cell.alertButton.hidden = YES;
        }
    } else {
        
        cell.chupiaoLabel.hidden = YES;
        cell.alertButton.hidden = YES;
        
        if (self.lotteryType == LotteryTypeLanCai) {
            
            cell.topSpaceCon.constant = 25 * iPhone6Ratio;

        } else if (self.lotteryType == LotteryTypeChuanTong) {
            
            cell.guoguanLabel.hidden = YES;
            cell.topSpaceCon.constant = 10 * iPhone6Ratio;

        }
    }
    if (self.contentHeaderTitleArray.count > 0) {
        cell.textLabel1.text = self.contentHeaderTitleArray[0];
        cell.textLabel2.text = self.contentHeaderTitleArray[1];
        cell.textLabel3.text = self.contentHeaderTitleArray[2];
        cell.textLabel4.text = self.contentHeaderTitleArray[3];
    }
    cell.tapBlock = ^ (id sender){
        //点击红圈圈
    };

}
- (void)configHeaderThreeCell:(CDContentHeaderThreeCell *)cell {
    
    if (self.lotteryType == LotteryTypeZuCai) {
        cell.topSpaceCon.constant = 41 * iPhone6Ratio;
        
        if (_lotteryId == 889) {
            //竞彩单关没有红球球
            cell.alertButton.hidden = YES;
        }
    } else {
        
        cell.chupiaoLabel.hidden = YES;
        cell.alertButton.hidden = YES;
        
        if (self.lotteryType == LotteryTypeLanCai) {
            
            cell.topSpaceCon.constant = 25 * iPhone6Ratio;

        } else if (self.lotteryType == LotteryTypeChuanTong) {

            cell.guoguanLabel.hidden = YES;
            cell.topSpaceCon.constant = 10 * iPhone6Ratio;

        }
    }
    if (self.contentHeaderTitleArray.count > 0) {
        cell.textLabel1.text = self.contentHeaderTitleArray[0];
        cell.textLabel2.text = self.contentHeaderTitleArray[1];
        cell.textLabel3.text = self.contentHeaderTitleArray[2];
        cell.textLabel4.text = self.contentHeaderTitleArray[3];
        cell.textLabel5.text = self.contentHeaderTitleArray[4];
    }
    if (self.lotteryId == 215 || self.lotteryId == 111 || self.lotteryId == 110) {
        //篮彩大小分,与四场进球,六场半全场的后三部分是等分的
        cell.label3WidthCon.constant = cell.width * 1 / 16;
        cell.label4WidthCon.constant = -cell.width * 1 / 16;
    }
   
    cell.tapBlock = ^ (id sender){
        //点击红圈圈
    };
    
}
#pragma mark - 配置内容 Cell 的显示
- (void)configContentOneCell:(CDContentOneCell *)cell indexPath:(NSIndexPath *)indexPath {
    //传统足球的场次区填的是编号1,2,3
    if (self.lotteryType == LotteryTypeChuanTong) {
        cell.changCiLabel.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    }
}
- (void)configContentTwoCell:(CDContentTwoCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    if (self.lotteryId == 215 ) {
        //篮彩大小分
        cell.ranFenWidthCon.constant = cell.width * 1 / 16;
        cell.touZhuWidthCon.constant = -cell.width * 1 / 16;
    }
    
}
- (void)configContentThreeCell:(CDContentThreeCell *)cell indexPath:(NSIndexPath *)indexPath {
    
}
- (void)configContentSiCJQCell:(CDSiCJQCell *)cell indexPath:(NSIndexPath *)indexPath {
    
    cell.num1Label.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row*2 - 1];
    cell.num2Label.text = [NSString stringWithFormat:@"%ld",(long)indexPath.row*2];
    cell.duiZhenLabel.text = [NSString stringWithFormat:@"足球\nVS\n篮球"];
    cell.touZhu1Label.text = @"0 1 2 3+";
    cell.touZhu2Label.text = @"投注2";
    cell.result1Label.text = @"彩果1";
    cell. result2Label.text = @"彩果2";
    if (self.lotteryId == 111) {
        //四场进球
        cell.type1Label.text = @"主";
        cell.type2Label.text = @"客";
    } else {
        //六场半全场
        cell.type1Label.text = @"半";
        cell.type2Label.text = @"全";
    }
}
- (CurrentLotteryType )getLotteryTypeWithLotteryId:(int)lotteryId {
    
    CurrentLotteryType type = LotteryTypeZuCai;
    
    if ([@[@(208),@(209),@(210),@(211),@(212),@(213),@(889)] containsObject:@(lotteryId)]) {
        type = LotteryTypeZuCai;
    } else if ([@[@(218),@(216),@(214),@(215),@(217),@(998)] containsObject:@(lotteryId)]) {
        type = LotteryTypeLanCai;
    } else if ([@[@(108),@(109),@(110),@(111)] containsObject:@(lotteryId)]) {
        type = LotteryTypeChuanTong;
    }
    
    return type;
    
}
@end
