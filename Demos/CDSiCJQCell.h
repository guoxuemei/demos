//
//  CDSiCJQCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/20.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  抄单的四场进球及六场半全场
 */
@interface CDSiCJQCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *num1Label;
@property (weak, nonatomic) IBOutlet UILabel *num2Label;
@property (weak, nonatomic) IBOutlet UILabel *duiZhenLabel;
@property (weak, nonatomic) IBOutlet UILabel *type1Label;
@property (weak, nonatomic) IBOutlet UILabel *type2Label;
@property (weak, nonatomic) IBOutlet UILabel *touZhu1Label;
@property (weak, nonatomic) IBOutlet UILabel *touZhu2Label;
@property (weak, nonatomic) IBOutlet UILabel *result1Label;
@property (weak, nonatomic) IBOutlet UILabel *result2Label;


@end
