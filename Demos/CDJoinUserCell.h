//
//  CDJoinUserCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/23.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  抄单用户 Cell
 */
@interface CDJoinUserCell : UITableViewCell

//参与用户,购买金额,出票状态,中奖金额,抄单佣金
@property (weak, nonatomic) IBOutlet UILabel *joinUserLabel;
@property (weak, nonatomic) IBOutlet UILabel *buyValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *ticketStateLabel;
@property (weak, nonatomic) IBOutlet UILabel *awardCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *yongJinLabel;


@end
