//
//  CDContentThreeCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/23.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDContentThreeCell.h"
#import "CDContentInsideCell.h"

@implementation CDContentThreeCell

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.3);
    CGContextSetRGBStrokeColor(context, 200.0/255.0, 200.0/255.0, 200.0/255.0, 1);
    
    CGContextMoveToPoint(context, self.contentView.left, self.contentView.bottom);
    CGContextAddLineToPoint(context, self.duiZhenLabel.right, self.contentView.bottom);
    
    CGContextMoveToPoint(context, self.changCiLabel.right, self.changCiLabel.top);
    CGContextAddLineToPoint(context, self.changCiLabel.right, self.contentView.bottom);
    
    CGContextMoveToPoint(context, self.duiZhenLabel.right, self.duiZhenLabel.top);
    CGContextAddLineToPoint(context, self.duiZhenLabel.right, self.contentView.bottom);

    
    CGContextStrokePath(context);
    
}
#pragma mark - UITableView Delegate and Datasource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.contentView.height / 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"CDContentInsideCell";
    CDContentInsideCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = (CDContentInsideCell *)[[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([CDContentInsideCell class]) owner:nil options:nil] lastObject];
        
    }
    cell.wanFaLabel.text = @"玩法 xx";
    cell.touZhuLabel.text = @"投注 xx";
    return cell;
    
}
@end
