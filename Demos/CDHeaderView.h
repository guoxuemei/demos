//
//  CDHeaderView.h
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CDHeaderView : UIView

//彩票种类图片
@property (weak, nonatomic) IBOutlet UIImageView *typeImageView;
//彩票种类
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//期号标签
@property (weak, nonatomic) IBOutlet UILabel *qihaoLabel;

//方案金额
@property (weak, nonatomic) IBOutlet UILabel *totalMoneyLabel;
//我的奖金
@property (weak, nonatomic) IBOutlet UILabel *myAwardLabel;
//方案奖金
@property (weak, nonatomic) IBOutlet UILabel *totalAwardLabel;
//佣金
@property (weak, nonatomic) IBOutlet UILabel *yongjinLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameConstraint;
//方案成功标签
@property (weak, nonatomic) IBOutlet UILabel *fangAnSucceed;
@end
