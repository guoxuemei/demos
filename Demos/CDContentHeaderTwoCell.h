//
//  CDContentHeaderTwoCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^AlertBtnTapBlock)(id sender);

/*!
 *  @brief  抄单内容头部,四列的情况:场次,主队 VS 客队,玩法,投注等
 */
@interface CDContentHeaderTwoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *guoguanLabel;
@property (weak, nonatomic) IBOutlet UILabel *chupiaoLabel;

//从左到右依次对应4个 label
@property (weak, nonatomic) IBOutlet UILabel *textLabel1;
@property (weak, nonatomic) IBOutlet UILabel *textLabel2;
@property (weak, nonatomic) IBOutlet UILabel *textLabel3;
@property (weak, nonatomic) IBOutlet UILabel *textLabel4;
@property (weak, nonatomic) IBOutlet UIButton *alertButton;
@property (nonatomic , copy) AlertBtnTapBlock tapBlock;
//场次距离方案内容的间距
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topSpaceCon;

@end
