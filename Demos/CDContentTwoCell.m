//
//  CDContentTwoCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDContentTwoCell.h"

@implementation CDContentTwoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.3);
    CGContextSetRGBStrokeColor(context, 200.0/255.0, 200.0/255.0, 200.0/255.0, 1);
    
    CGContextMoveToPoint(context, self.changCiLabel.right, self.changCiLabel.top );
    CGContextAddLineToPoint(context, self.changCiLabel.right, self.changCiLabel.bottom);
    
    CGContextMoveToPoint(context, self.duiZhenLabel.right, self.duiZhenLabel.top );
    CGContextAddLineToPoint(context, self.duiZhenLabel.right, self.duiZhenLabel.bottom);
    
    CGContextMoveToPoint(context, self.ranFenLabel.right, self.ranFenLabel.top );
    CGContextAddLineToPoint(context, self.ranFenLabel.right, self.ranFenLabel.bottom);
    
    CGContextMoveToPoint(context, self.touZhuLabel.right, self.touZhuLabel.top );
    CGContextAddLineToPoint(context, self.touZhuLabel.right, self.touZhuLabel.bottom);
    
    CGContextMoveToPoint(context, self.saiGuoLabel.right, self.saiGuoLabel.top );
    CGContextAddLineToPoint(context, self.saiGuoLabel.right, self.saiGuoLabel.bottom);
    
    CGContextMoveToPoint(context, self.contentView.left, self.contentView.bottom);
    CGContextAddLineToPoint(context, self.contentView.right, self.contentView.bottom);
    
    CGContextMoveToPoint(context, self.contentView.left, self.contentView.top);
    CGContextAddLineToPoint(context, self.contentView.right, self.contentView.top);
    
    CGContextStrokePath(context);
    
}
@end
