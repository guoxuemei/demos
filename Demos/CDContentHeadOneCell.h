//
//  CDContentHeadOneCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  抄单内容头部,未公开 Cell
 */
@interface CDContentHeadOneCell : UITableViewCell

@end
