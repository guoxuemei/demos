//
//  CDContentInsideCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/23.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDContentInsideCell.h"

@implementation CDContentInsideCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.3);
    CGContextSetRGBStrokeColor(context, 200.0/255.0, 200.0/255.0, 200.0/255.0, 1);
    
    CGContextMoveToPoint(context, self.contentView.left, self.contentView.bottom);
    CGContextAddLineToPoint(context, self.contentView.right, self.contentView.bottom);
    
    CGContextMoveToPoint(context, self.wanFaLabel.right, self.wanFaLabel.top);
    CGContextAddLineToPoint(context, self.wanFaLabel.right, self.wanFaLabel.bottom);
    
    CGContextStrokePath(context);
    
}
@end
