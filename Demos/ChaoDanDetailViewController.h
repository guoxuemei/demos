//
//  ChaoDanDetailViewController.h
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

//详情页的所有者
typedef NS_ENUM(NSInteger , UserType){
    
    UserTypeFaQiRen = 0 ,   //抄单的发起人
    UserTypeChaoDanRen ,     //抄单人
    
};

//当前抄单开放情况
typedef NS_ENUM(NSInteger , OpenType) {
    
    OpenTypePublic = 0 ,    //公开,针对发起人及截止后的抄单人
    OpenTypeNonPublic ,     //非公开,截止后公开
    
};

/*!
 *  @brief  当前的彩种类型,分为竞彩,篮彩,传统足球三种
 */
typedef NS_ENUM(NSInteger , CurrentLotteryType){
    
    LotteryTypeZuCai = 0,//足彩
    LotteryTypeLanCai ,//篮彩
    LotteryTypeChuanTong //传统足球
};

///*!
// *  开奖与未开奖
// */
//typedef NS_ENUM(NSInteger, AwardType){
//    
//    AwardTypeOpen = 0,  //已开奖
//    AwardTypeClose ,    //未开奖
//    
//} ;
//
///*!
// *  中奖或未中奖
// */
//typedef NS_ENUM(NSInteger , BonusType){
//    
//    BonusTypeWinning = 0,//中奖
//    BonusTypeLose ,      //未中奖
//    
//} ;
@interface ChaoDanDetailViewController : UIViewController

@property (nonatomic , assign) UserType userType;
@property (nonatomic , assign) OpenType openType;
@property (nonatomic , assign) CurrentLotteryType lotteryType;
//@property (nonatomic , assign) AwardType awardType;
//@property (nonatomic , assign) BonusType bonusType;
@property (nonatomic , assign) int lotteryId;
@end
