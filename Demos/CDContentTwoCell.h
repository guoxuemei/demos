//
//  CDContentTwoCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  五列的赛事信息 对应CDContentHeaderThreeCell
 */
@interface CDContentTwoCell : UITableViewCell

//场次
@property (weak, nonatomic) IBOutlet UILabel *changCiLabel;
//对阵
@property (weak, nonatomic) IBOutlet UILabel *duiZhenLabel;
//让分
@property (weak, nonatomic) IBOutlet UILabel *ranFenLabel;
//投注
@property (weak, nonatomic) IBOutlet UILabel *touZhuLabel;
//赛果
@property (weak, nonatomic) IBOutlet UILabel *saiGuoLabel;
//让分的宽约束与,投注的宽约束
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ranFenWidthCon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *touZhuWidthCon;

@end
