//
//  CDHeaderView.m
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDHeaderView.h"

@implementation CDHeaderView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {

    //一条大横线,两条竖短线
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.3);
    CGContextSetRGBStrokeColor(context, 200.0/255.0, 200.0/255.0, 200.0/255.0, 1);
    
    CGContextMoveToPoint(context, 0, 50*iPhone6Ratio);
    CGContextAddLineToPoint(context, self.right, 50*iPhone6Ratio);
    
    CGContextMoveToPoint(context, self.width / 3, 50*iPhone6Ratio);
    CGContextAddLineToPoint(context, self.width / 3, self.bottom);
    
    CGContextMoveToPoint(context, self.width / 3 * 2, 50*iPhone6Ratio);
    CGContextAddLineToPoint(context, self.width / 3 * 2, self.bottom);

    
    CGContextStrokePath(context);
    

}
- (void)awakeFromNib {

    self.fangAnSucceed.layer.borderWidth = 2;
    self.fangAnSucceed.layer.borderColor = [UIColor greenColor].CGColor;
    self.fangAnSucceed.transform = CGAffineTransformMakeRotation(- M_PI_4);
}

@end
