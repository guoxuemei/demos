//
//  CDContentOneCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @brief  四列的赛事信息,对应 CDContentHeaderTwoCell
 */
@interface CDContentOneCell : UITableViewCell

//场次
@property (weak, nonatomic) IBOutlet UILabel *changCiLabel;
//对阵
@property (weak, nonatomic) IBOutlet UILabel *duiZhenLabel;
//玩法
@property (weak, nonatomic) IBOutlet UILabel *wanFaLabel;
//投注
@property (weak, nonatomic) IBOutlet UILabel *touZhuLabel;
@end
