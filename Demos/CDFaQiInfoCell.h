//
//  CDFaQiInfoCell.h
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CDRecordBlock)(void) ;

/*!
 *  @brief  抄单详情的发起人信息 Cell
 */
@interface CDFaQiInfoCell : UITableViewCell

//发起人名称
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
//中奖金额
@property (weak, nonatomic) IBOutlet UILabel *awardCountLabel;
//抄单记录按钮
@property (weak, nonatomic) IBOutlet UIButton *cdRecordBtn;

@property (nonatomic , copy)CDRecordBlock recordBlock;

@end
