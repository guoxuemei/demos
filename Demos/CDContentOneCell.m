//
//  CDContentOneCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/19.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDContentOneCell.h"

@implementation CDContentOneCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.5);
    CGContextSetRGBStrokeColor(context, 200.0/255.0, 200.0/255.0, 200.0/255.0, 1);
    
    CGContextMoveToPoint(context, self.changCiLabel.right, self.changCiLabel.top );
    CGContextAddLineToPoint(context, self.changCiLabel.right, self.changCiLabel.bottom);
    
    CGContextMoveToPoint(context, self.duiZhenLabel.right, self.duiZhenLabel.top );
    CGContextAddLineToPoint(context, self.duiZhenLabel.right, self.duiZhenLabel.bottom);
    
    CGContextMoveToPoint(context, self.wanFaLabel.right, self.wanFaLabel.top );
    CGContextAddLineToPoint(context, self.wanFaLabel.right, self.wanFaLabel.bottom);
    
    CGContextMoveToPoint(context, self.contentView.left, self.contentView.bottom);
    CGContextAddLineToPoint(context, self.contentView.right, self.contentView.bottom);
    CGContextStrokePath(context);
    
}
@end
