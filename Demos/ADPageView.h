//
//  ADPageView.h
//  Demos
//
//  Created by lixiaofei on 15/11/16.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import <UIKit/UIKit.h>

//点击广告
typedef void (^TapADBlock)(NSInteger currentIndex);

@interface ADPageView : UIView<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic ,strong) NSMutableArray *itemsArray;
@property (nonatomic ,copy) TapADBlock  tapBlock;

@end
