//
//  ADPageView.m
//  Demos
//
//  Created by lixiaofei on 15/11/16.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "ADPageView.h"
#import "ADCollectionViewCell.h"

#define Multi  10//倍数

@interface ADPageView ()

@property (nonatomic ,strong) UICollectionView *collectionView;
@property (nonatomic ,strong) UIPageControl    *pageControl;
@property (nonatomic ,assign) NSInteger         currentPage;

@property (nonatomic ,strong) NSTimer *timer;

@end

@implementation ADPageView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        _itemsArray = [[NSMutableArray alloc] init];
        [self initView];
    }
    
    return self;
}
- (void)initView {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(SCREEN_WIDTH, 64);
    layout.minimumLineSpacing = 0;//水平滚动下,两个item 的间距是行间距

    self.collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];//collectionView的高度至少要大于 itemCell 的高度,否则显示不出来😭
    self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    self.collectionView.bounces = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView registerClass:[ADCollectionViewCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
    [self addSubview:self.collectionView];
    
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.backgroundColor = [UIColor clearColor];
    self.pageControl.currentPage = 0;
    self.pageControl.frame = CGRectMake(0, 44, SCREEN_WIDTH, 20);
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    self.pageControl.pageIndicatorTintColor = [UIColor grayColor];
    self.pageControl.userInteractionEnabled = NO;//不让它响应点击
    [self addSubview:self.pageControl];
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(scrollAd) userInfo:nil repeats:YES];
    
}

- (void)scrollAd {
    
    //先判断有没有到最后一页,到最后一页先滚回到中部,再调 setContentOffset:...
    CGPoint oldOffset = self.collectionView.contentOffset;
    [self setCollectionViewContentOffset:oldOffset];
    
    CGPoint newOffset = CGPointMake(self.collectionView.contentOffset.x + CGRectGetWidth(self.collectionView.frame), self.collectionView.contentOffset.y);

    [self.collectionView setContentOffset:newOffset animated:YES];
    [self updateCurrentPageWithOffset:newOffset];
}

#pragma mark - UICollectionView Delegate and Datasource Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _itemsArray.count * Multi;//复用两个 cell, 一直在滚动啊滚动
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"CellIdentifier";
    ADCollectionViewCell *cell = (ADCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSString *imageURL = [NSString stringWithFormat:@"%ld.jpg",indexPath.item % _itemsArray.count];
    cell.imageURLBlock = ^{
        return imageURL;
    };
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = indexPath.item % _itemsArray.count;
    if (_tapBlock) {
        _tapBlock(index);
    }

}
//在还没有拖拽的时候,判断是否需要滚回到中部
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {

    CGPoint contentOffset = self.collectionView.contentOffset;
    [self setCollectionViewContentOffset:contentOffset];

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    [self updateCurrentPageWithOffset:scrollView.contentOffset];
    
}

//设置滚动一页
- (void)setCollectionViewContentOffset:(CGPoint )contentOffset {
    if (contentOffset.x >= (_itemsArray.count*Multi-1)*SCREEN_WIDTH) {
        NSLog(@"到底了");//直接滚动到头,然后用户再继续翻
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:Multi / 2 * _itemsArray.count-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    }
    if (contentOffset.x <= 0) {
        NSLog(@"到头了");
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:Multi / 2 * _itemsArray.count inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        
    }
}
- (void)updateCurrentPageWithOffset:(CGPoint)contentOffset {

    int currpage = fabs(contentOffset.x) / SCREEN_WIDTH;
    self.currentPage = currpage % _itemsArray.count;
    
}
#pragma mark - Setters Methods
- (void)setCurrentPage:(NSInteger)currentPage {
    
    if (_currentPage != currentPage) {
        _currentPage = currentPage;
        self.pageControl.currentPage = currentPage;
    }
    
}

- (void)setItemsArray:(NSMutableArray *)itemsArray {
    
    _itemsArray = itemsArray;
    self.pageControl.hidden = (_itemsArray.count == 0) ? YES : NO;
    self.pageControl.numberOfPages = _itemsArray.count;
    [self.collectionView reloadData];
    if (_itemsArray.count > 0) {
        [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:Multi / 2 * _itemsArray.count inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
    }
}
@end
