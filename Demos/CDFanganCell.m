//
//  CDFanganCell.m
//  Demos
//
//  Created by lixiaofei on 15/11/18.
//  Copyright © 2015年 lxf. All rights reserved.
//

#import "CDFanganCell.h"

@implementation CDFanganCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, 0.3);
    CGContextSetRGBStrokeColor(context, 200.0/255.0, 200.0/255.0, 200.0/255.0, 1);
    
    CGContextMoveToPoint(context, self.contentView.left, self.startLabel.bottom + 2.5);
    CGContextAddLineToPoint(context, self.contentView.right, self.startLabel.bottom + 2.5);
    CGContextStrokePath(context);
    
}
@end
